function display_twitter_plea() {
    $("#twitter-follow-plea").slideDown("medium");
    setTimeout(function() {$("#book-endorsement-container").one('mouseout', hide_twitter_plea);}, 500);
}
function hide_twitter_plea() {
    $("#twitter-follow-plea").slideUp("medium");
    setTimeout(function() {$("#book-endorsement-container").one('mouseover', display_twitter_plea);}, 500);
}

function on_page_scroll() {
    let curr_scroll_pos = $(this).scrollTop();
    if (curr_scroll_pos > old_scroll_pos) {
        //SCROLLING DOWN
        let els = $(".topbar-element-large");
        els.addClass("topbar-element-small");
        $("#topbar-wrapper").addClass("topbar-wrapper-small");
        $("#topbar-wrapper").removeClass("topbar-wrapper-large");
    }
    else {
        //SCROLLING UP
        let els = $(".topbar-element-small");
        els.removeClass("topbar-element-small");
        $("#topbar-wrapper").addClass("topbar-wrapper-large");
        $("#topbar-wrapper").removeClass("topbar-wrapper-small");
    }
    old_scroll_pos = curr_scroll_pos;
}

/////////////
// DEFINE GLOBAL VARS
////////////

let old_scroll_pos = 0;

$(document).ready(function() {
    $("#book-endorsement-container").one('mouseover', display_twitter_plea);
    $(window).on("scroll", on_page_scroll);
});
